### Jitsihawk - Jitsi Meet log analysis tool

##### Logsnatch v.0.1
##### Jitsihawk v.0.22

![](images/jitsihawk.png)

##### Description
The Jitsihawk project aims to give system administrators a straighforward cli-tool for the analysis
of the current state of a Jitsi Meet server. While monitoring approaches using the Prosody server and
lua scripting are a viable option, the Jitsihawk takes a different approach to monitoring and uses grep and regular expressions to analyse the Jicofo log files.

Jitsihawk provides the following information about rooms on a Jitsi Meet server:
- currently opened rooms count
- currently opened rooms names
- number of participants per room
- last host (authenticated user) that entered the room

The project is still in an early state and there are currently the following limitations:
- only room names formatted as: `roomname`, `roomname-roomname`, `roomname_roomname` are currently supported.
- only the last person with host priviledges that entered the room is shown as host (not necessarily the same
person that actually authenticated the room).

##### Usage
Clone the git repository to your system and run (you need access to /var/log/jitsi, so use sudo or priviledges):  
`./main -s $YOURSERVERDOMAIN`  
For example:  
`./main -s meet.jitsi.org`  

What will happen:  
The main script will call the logsnatch script that will copy the log files from /var/log/jitsi and 
combine them together into jicofo.log.total in the lib directory. This will then be analyzed by 
Jitsihawk and Jitsihawk will report the state of the server back through the CLI.


##### How it works
The program consists of two working parts: logsnatch & jitsihawk.  
The logsnatch program is responsible for combining all previous jicofo log files together into jicofo.log.total.  
The jitsihawk program uses grep to pull specific log entries from the log file using regular expressions.  
Internally jitsihawk stores the results of these grep outputs into two main associative bash arrays: roomArray & hostArray.  
For convenience the script uses a series of utility functions for each of these two associative arrays.
It is roomArray that is ultimately used to output the results. 
